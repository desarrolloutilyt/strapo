import Link from "next/link";

const ProductCard = ({ Href, Title, img, Price }) => (
  <Link href={Href}>
    <a className="relative flexcol-c-c bg-white shadow-xl p-0_75 rounded-lg transform hover:scale-105">
      <img className="w-5/6" src={`http://localhost:3020${img.url}`}></img>
      <h3 className="text-center text-sm leading-4 h-2 overflow-hidden mt-0_25">
        {Title}
      </h3>
      <div className="mt-0_5 mb-0_75">
        <span className="text-2xl text-black font-bold">{Price}€</span>
      </div>
      <button className="font-medium bg-black text-white rounded-lg text-lg py-0_375 px-2">
        Comprar
      </button>
    </a>
  </Link>
);

export default ProductCard;
