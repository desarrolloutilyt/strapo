import ProductCard from "./productCard";

const ProductGrid = ({ products }) => (
  <div className="container-lg flex-c-c flex-wrap">
    {products.map((i, index) => (
      <div
        key={index}
        className="xs:w-1/2 sm:w-1/3 mdlg:w-1/4 xssm:p-0_25 mdlg:p-0_5 hover:p-0"
      >
        <ProductCard {...i} />
      </div>
    ))}
  </div>
);

export default ProductGrid;
