import Head from "next/head";

import ProductGrid from "../components/products/productGrid";

export default function Home({ products }) {
  return (
    <div>
      <Head>
        <title>Listado de productos</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="container-xl">
        <ProductGrid products={products} />
      </div>
    </div>
  );
}

export const getServerSideProps = async () => {
  const response = await fetch("http://localhost:3020/products");
  const products = await response.json();
  return { props: { products } };
};
