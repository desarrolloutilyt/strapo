const colors = {
  black: "#000000",
  white: "#FFFFFF",
};

module.exports = {
  purge: {
    mode: "all",
    content: ["./components/**/*.js", "./pages/**/*.js"],
  },
  theme: {
    colors,
    fill: colors,
    extend: {},
    screens: {
      xs: { max: "480px" },
      sm: { min: "481px", max: "768px" },
      md: { min: "769px", max: "1024px" },
      lg: { min: "1025px" },
      xssm: { max: "768px" },
      smmd: { min: "481px", max: "1024px" },
      mdlg: { min: "769px" },
    },
  },
  variants: {},
  plugins: require("tailwindcssdu"),
};
